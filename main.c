#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>

#define CHILD_PID 0
#define MIN_ARGC 2


int main(int argc, char ** argv) {
    if (argc < MIN_ARGC) {
        perror("ERROR: not enough arguements\n");
        return EXIT_FAILURE;
    }

    char **command_args = &argv[1];
    char *command = argv[1];
    pid_t pid = fork();

    if (pid == -1) {
        perror("ERROR: error in fork()\n");
        return EXIT_FAILURE;
    }

    if (pid == CHILD_PID) {
        execvp(command, command_args);
        perror("ERROR: error in execvp\n");
        return EXIT_FAILURE;
    }

    int wait_status;
    if (wait(&wait_status) == -1)
    {
        perror("ERROR: error in wait()\n");
        return EXIT_FAILURE;
    }
    if (WIFEXITED(wait_status))
        printf("\nChild process finished with exit code %d\n", WEXITSTATUS(wait_status));

    if (WIFSIGNALED(wait_status))
        printf("\nChild process ended with signal %d\n", WTERMSIG(wait_status));

    return EXIT_SUCCESS;
}
